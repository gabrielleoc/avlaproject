package com.avla.ProyectoAvla;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.avla.ProyectoAvla.model.Users;
import com.avla.ProyectoAvla.repository.IUsersRepository;

@SpringBootTest
class ProyectoAvlaApplicationTests {

	@Autowired
	private IUsersRepository repoUser;
	
	@Test
	void contextLoads() {
		
		Users user1 = new Users();
		user1.setUser("gaby");
		user1.setPass("1234567");
		repoUser.save(user1);
		
		Users user2 = new Users();
		user2.setUser("floki");
		user2.setPass("1234567");
		repoUser.save(user2);
		
		Users user3 = new Users();
		user3.setUser("avla");
		user3.setPass("1234567");
		repoUser.save(user3);
		
	}

}
