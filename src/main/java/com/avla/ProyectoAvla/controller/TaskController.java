package com.avla.ProyectoAvla.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.avla.ProyectoAvla.dto.NewTaskRequest;
import com.avla.ProyectoAvla.model.Tasks;
import com.avla.ProyectoAvla.model.Users;
import com.avla.ProyectoAvla.repository.ITasksRepository;
import com.avla.ProyectoAvla.repository.IUsersRepository;
import com.avla.ProyectoAvla.service.TaskService;

@RestController
@RequestMapping("/task")
public class TaskController {
	
	@Autowired
	private ITasksRepository taskRepo;
	
	@Autowired
	private TaskService taskService;
	
	@Autowired
	private IUsersRepository userRepo;
	
	
	@GetMapping("/users")
	public ResponseEntity<List<Users>> listUsers(){
		
		List<Users> listPage = userRepo.findAll();
		
		return ResponseEntity.ok(listPage);
	}
	
	
	@GetMapping
	public ResponseEntity<List<Tasks>> listUsersWithTasks(){
		
		List<Tasks> listPagesw = taskRepo.findAll();
		
		return ResponseEntity.ok(listPagesw);
	}
	
	
	@PostMapping
	public ResponseEntity<Tasks> newTask(@Valid @RequestBody NewTaskRequest newTaskRequest){
		
		Tasks tarea = new Tasks();  
		tarea.setName(newTaskRequest.getName());
		tarea.setDescription(newTaskRequest.getDescription());
		tarea.setColumState("TODO");
		
		Users user =  taskService.getUserWithLessTask();
		tarea.setUser(user);
		
		Tasks tareaGuardada = taskRepo.save(tarea); 
		
		return ResponseEntity.ok(tareaGuardada);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteTask(@PathVariable("id") int id){
	
		taskRepo.deleteById(id);
		return ResponseEntity.ok().build();
	}
	
	
	@PutMapping("/{id}")
	public ResponseEntity<Tasks> updateTask(@PathVariable("id") int id, @Valid @RequestBody NewTaskRequest newTaskRequest){

		Optional<Tasks> optionalTarea = taskRepo.findById(id);
		if(optionalTarea.isPresent()) {
			Tasks updateTask = optionalTarea.get();
			updateTask.setName(newTaskRequest.getName());
			updateTask.setDescription(newTaskRequest.getDescription());
			
			
			if (newTaskRequest.getUserid() > 0) {
				
				Optional<Users> optionalUser = userRepo.findById(newTaskRequest.getUserid());
				
				if (optionalUser.isPresent()) {
					
					updateTask.setUser(optionalUser.get());
					
				}else {
					return ResponseEntity.notFound().build();
				}
			}
			
			taskRepo.save(updateTask); 
			
			return ResponseEntity.ok(updateTask);
		}else {
			
			return ResponseEntity.notFound().build();
		}
		
		
	}
	
	@PutMapping("/done/{id}")
	public ResponseEntity<Tasks> updateTaskDone(@PathVariable("id") int id){
		
		Optional<Tasks> optionalTarea = taskRepo.findById(id);
		if(optionalTarea.isPresent()) {
			Tasks updateTask = optionalTarea.get();
			updateTask.setColumState("DONE");
			taskRepo.save(updateTask); 
			
			return ResponseEntity.ok(updateTask);
		}else {
			
			return ResponseEntity.notFound().build();
		}
		
	}
	
	@PutMapping("/doing/{id}")
	public ResponseEntity<Tasks> updateTaskDoing(@PathVariable("id") int id){
		
		Optional<Tasks> optionalTarea = taskRepo.findById(id);
		if(optionalTarea.isPresent()) {
			Tasks updateTask = optionalTarea.get();
			updateTask.setColumState("DOING");
			taskRepo.save(updateTask); 
			
			return ResponseEntity.ok(updateTask);
		}else {
			
			return ResponseEntity.notFound().build();
		}
		
	}
	

}
