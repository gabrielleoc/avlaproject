package com.avla.ProyectoAvla.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.avla.ProyectoAvla.model.Users;

@Repository
public interface IUsersRepository extends JpaRepository<Users, Integer>{

}
