package com.avla.ProyectoAvla.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.avla.ProyectoAvla.model.Tasks;

@Repository
public interface ITasksRepository extends JpaRepository<Tasks, Integer>{

	List<Tasks> findByUserId(int userId);
}
