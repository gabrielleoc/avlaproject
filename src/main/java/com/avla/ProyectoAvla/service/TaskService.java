package com.avla.ProyectoAvla.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avla.ProyectoAvla.model.Tasks;
import com.avla.ProyectoAvla.model.Users;
import com.avla.ProyectoAvla.repository.ITasksRepository;
import com.avla.ProyectoAvla.repository.IUsersRepository;


@Service
public class TaskService {

	@Autowired
	private ITasksRepository repoTask;
	@Autowired
	private IUsersRepository repoUsers;
	
	public Users getUserWithLessTask() {
		
		List<Users> listUsers = repoUsers.findAll();
		
		int count = 0;
		Users freeUser = null;
		
		for (Users users : listUsers) {
			
			List<Tasks> userTasks = repoTask.findByUserId(users.getId());
			if(freeUser == null) {
				count = userTasks.size();
				freeUser = users;
			}
			if (count >= userTasks.size()) {
				count = userTasks.size();
				freeUser = users;
			}
			
		}
		
		return freeUser;
		
	}
	
	
}
