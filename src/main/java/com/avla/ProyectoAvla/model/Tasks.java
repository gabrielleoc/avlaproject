package com.avla.ProyectoAvla.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
public class Tasks {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(length = 20, nullable = false)
	@Size(min = 1, max = 20)
	@NotEmpty
	private String name;
	@Column(length = 50, nullable = false)
	@Size(min = 1, max = 50)
	@NotEmpty
	private String description;

	@Column(length = 100, nullable = false)
	@NotEmpty
	private String columState;
	@ManyToOne
	private Users user;
	
	
	public Tasks() {
		super();
	}


	public Tasks(int id, String name, String description, String columState) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.columState = columState;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	
	
	public String getColumState() {
		return columState;
	}


	public void setColumState(String columState) {
		this.columState = columState;
	}


	public Users getUser() {
		return user;
	}


	public void setUser(Users user) {
		this.user = user;
	}


	@Override
	public String toString() {
		return "Tasks [id=" + id + ", name=" + name + ", description=" + description + ", columState=" + columState
				+ "]";
	}

	
	
	
	
}
