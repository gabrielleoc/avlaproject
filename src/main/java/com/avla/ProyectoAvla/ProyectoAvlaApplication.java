package com.avla.ProyectoAvla;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoAvlaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoAvlaApplication.class, args);
	}

}
