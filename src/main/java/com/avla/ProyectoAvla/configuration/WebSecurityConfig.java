package com.avla.ProyectoAvla.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebSecurityConfig {

  
    @Bean
    CorsFilter corsFilter() {
        return new CorsFilter("http://localhost:3000");
    }

}