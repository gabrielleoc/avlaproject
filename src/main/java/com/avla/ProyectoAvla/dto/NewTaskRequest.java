package com.avla.ProyectoAvla.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

public class NewTaskRequest implements Serializable{

	@NotEmpty
	private String name;
	@NotEmpty
	private String description;
	private int userid;
	
	public NewTaskRequest() {
		super();
	}


	public NewTaskRequest(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}
	
	


	public NewTaskRequest(@NotEmpty String name, @NotEmpty String description, int userid) {
		super();
		this.name = name;
		this.description = description;
		this.userid = userid;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public int getUserid() {
		return userid;
	}


	public void setUserid(int userid) {
		this.userid = userid;
	}
	
	


	
}
